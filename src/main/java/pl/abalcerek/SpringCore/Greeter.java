package pl.abalcerek.SpringCore;

public class Greeter {

    private String greeting;

    public Greeter(String greeting) {
        this.greeting = greeting;
    }

    void greet() {
        System.out.println(greeting);
    }

}
