package pl.abalcerek.SpringCore;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@Slf4j
public class SpringCoreApplication {

	@Bean(name = "polishGreeting")
	public String polishGreeting() {
		return "czesc";
	}

	@Bean(name = "englishGreeter")
	public String englishGreeter() {
		return "hello";
	}

    @Bean()
    public Greeter greeter(@Qualifier("polishGreeting") String greeting) {
        return new Greeter(greeting);
    }


    public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(SpringCoreApplication.class, args);

		Greeter greeter = context.getBean(Greeter.class);
        greeter.greet();
	}

}
